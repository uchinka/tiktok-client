import Vue from 'vue'
import Router from 'vue-router'
import HomeComponent from '@/components/HomeComponent'
import ChallengeComponent from '@/components/ChallengeComponent'
import MusicComponent from '@/components/MusicComponent'
import UserComponent from '@/components/UserComponent'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomeComponent',
      component: HomeComponent,
      meta: {
        title: '#tiktokotkit'
      }
    },
    {
      path: '/challenge/:id',
      name: 'ChallengeComponent',
      component: ChallengeComponent
    },
    {
      path: '/music/:id',
      name: 'MusicComponent',
      component: MusicComponent
    },
    {
      path: '/user/:id',
      name: 'UserComponent',
      component: UserComponent
    }
  ]
})
